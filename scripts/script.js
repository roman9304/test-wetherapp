const daysData = [
{ date: 0000000000000, temperature: { night: getRandomTemp(), day: getRandomTemp(), }, cloudiness: getRandomcloud(), snow: getRandomWeth(), rain: getRandomWeth(), },
{ date: 0000000000000, temperature: { night: getRandomTemp(), day: getRandomTemp(), }, cloudiness: getRandomcloud(), snow: getRandomWeth(), rain: getRandomWeth(), },
{ date: 0000000000000, temperature: { night: getRandomTemp(), day: getRandomTemp(), }, cloudiness: getRandomcloud(), snow: getRandomWeth(), rain: getRandomWeth(), },
{ date: 0000000000000, temperature: { night: getRandomTemp(), day: getRandomTemp(), }, cloudiness: getRandomcloud(), snow: getRandomWeth(), rain: getRandomWeth(), },
{ date: 0000000000000, temperature: { night: getRandomTemp(), day: getRandomTemp(), }, cloudiness: getRandomcloud(), snow: getRandomWeth(), rain: getRandomWeth(), },
{ date: 0000000000000, temperature: { night: getRandomTemp(), day: getRandomTemp(), }, cloudiness: getRandomcloud(), snow: getRandomWeth(), rain: getRandomWeth(), },
{ date: 0000000000000, temperature: { night: getRandomTemp(), day: getRandomTemp(), }, cloudiness: getRandomcloud(), snow: getRandomWeth(), rain: getRandomWeth(), },
{ date: 0000000000000, temperature: { night: getRandomTemp(), day: getRandomTemp(), }, cloudiness: getRandomcloud(), snow: getRandomWeth(), rain: getRandomWeth(), },
{ date: 0000000000000, temperature: { night: getRandomTemp(), day: getRandomTemp(), }, cloudiness: getRandomcloud(), snow: getRandomWeth(), rain: getRandomWeth(), },
{ date: 0000000000000, temperature: { night: getRandomTemp(), day: getRandomTemp(), }, cloudiness: getRandomcloud(), snow: getRandomWeth(), rain: getRandomWeth(), }
];

function getRandomTemp() {
    const min= -8;
    const max= 10;
    return Math.round(Math.random() * (max - min) + min);
  }

  function getRandomWeth() {
  if (Math.round(Math.random()) >0.5) {
    return true;
  }
    return false;
  }
  function getRandomcloud() {
    if (Math.round(Math.random()) >0.5) {
      return 'Ясно';
    }
      return 'Облачно';
    }

  function getNormilizeData(data) {
    let preparedData = [];
    let dateNow = new Date();
    for (let key in data) {
      preparedData.push(Object.assign(data[key], { date: dateNow}));
      dateNow = new Date(dateNow.setDate(dateNow.getDate()+1));
    }
    return preparedData;
}

function getDayOfWeekName(date) {
  const thisDate = date.date;
  const dateNow = new Date();
  if ((dateNow.getFullYear() === thisDate.getFullYear() && dateNow.getMonth() === thisDate.getMonth() && dateNow.getDate() === thisDate.getDate())) {
    return "Сегодня";
  } else {
    thisDate.getMonth();
    const dayWeekNames = ["Воскресенье", "Понедельник", "вторник", "Среда", "Четверг", "Пятница", "Суббота"];
    return `${dayWeekNames[thisDate.getDay()]}`;
  }
}

function getTemperature(dayData) {
  const temperature = {
    tempDay: dayData.temperature.day,
    tempNight: dayData.temperature.night
  }
  return temperature;
}

function getPrecipitation(dayData) {
  let precipitation = null;
  if (dayData.snow) {
    precipitation = "Снег";
  }
  if (dayData.rain) {
    precipitation = "Дождь";
  }
  if (dayData.snow && dayData.rain) {
    precipitation = "Снег с дождем";
  }
  if (precipitation===null) {
    precipitation = "";
  }
  if (dayData.snow || dayData.rain) {
    dayData.cloudiness = "";
  }
  return `${precipitation} ${dayData.cloudiness}`;
}

function getDateInfo(dayData) {
  const monthNumber = dayData.date.getMonth();
  const dayNum = dayData.date.getDate();
  const monthNames = ["января", "февраля", "марта", "апреля", "мая", "июня",
    "июля", "августа", "сентября", "октября", "ноября", "декабря"];
  return `${dayNum} ${monthNames[monthNumber]}`;
}

function getDayHtml(dayData) {
  const dayOfWeekName = getDayOfWeekName(dayData);
  const dateInfo = getDateInfo(dayData);
  const imgUrl = getImageUrl(dayData);
  const temperature = getTemperature(dayData);
  const precipitation = getPrecipitation(dayData);
  const innerBlock =
    `<div class="day-block">
            <div class="day-number text-Property">${dayOfWeekName}</div>
            <div class="day-day text-Property">${dateInfo}</div>
            <div class="day-icon-wrapper">
                <img class="day-icon" alt="иконка погоды" src="${imgUrl}" />
            </div>
            <div class="day-day-temperature text-Property"> Днем ${temperature['tempDay']}°</div>
            <div class="day-night-temperature text-Property">Ночью ${temperature['tempNight']}°</div>
            <div class="day-precipitation text-Property">${precipitation}</div>
        </div>`;
  return innerBlock;
}

function getDaysHtml(daysData) {
  const data = getNormilizeData(daysData);
  let days = '';
  for (let key in data) {
  }
  data.forEach(function (dayData) {
    if (getDayHtml(dayData) != undefined) {
      days += getDayHtml(dayData);
    }
  });
  return days;
}
function getImageUrl(dayData) {
  const dayIcons = makeDayIcon();
  if (dayData.cloudiness === 'Облачно') {
    dayData.imgUrl = dayIcons['cloudy'];
  }
  if (dayData.cloudiness === 'Ясно') {
    dayData.imgUrl = dayIcons['sunny'];
  }
  if (dayData.snow) {
    dayData.imgUrl = dayIcons['snow'];
  }
  if (dayData.rain) {
    dayData.imgUrl = dayIcons['rain'];
  }
  if (dayData.snow && dayData.rain) {
    dayData.imgUrl = dayIcons['snowAndRain'];
  }
  return dayData.imgUrl;
}

function makeDayIcon() {
  const dayIcons = {
    cloudy: "./Images/weather/cloudy.png",
    rain: "./Images/weather/rain.png",
    snow: "./Images/weather/snow.png",
    sunny: "./Images/weather/sunny.png",
    snowAndRain: "./Images/weather/snowAndRain.png"
  }
  return dayIcons;
}

function createButtons() {
  var leftButton = document.createElement('div');
  var rightButton = document.createElement('div');
  const propertyButtonImg = `./Images/button/button.png class="button"`;
  const leftButtonHtml = `<div id="buttonLeft"><img src=${propertyButtonImg}></div>`;
  const rightButtonHtml = `<div id="buttonRight"><img src=${propertyButtonImg}></div>`;
  leftButton.innerHTML = leftButtonHtml;
  rightButton.innerHTML = rightButtonHtml;
  const weatherApp = document.getElementById('weatherApp');
  weatherApp.insertBefore(leftButton, weatherApp.firstChild);
  weatherApp.appendChild(rightButton);
}

function run() {
  const container = document.getElementById('container');
  const daysHtml = getDaysHtml(daysData);
  container.innerHTML = daysHtml;
  createButtons();
}

function shift(numbShift) {
  const dayBlock = document.getElementsByClassName("day-block");
  for (let i = 0; i < dayBlock.length; i++) {
    dayBlock[i].style.transform = `translateX(${numbShift}px)`;
  }
}

run();
let tempShift = 0;
const numbShift = 142;
let ind = 0;

buttonLeft.onclick = function () {
  if (tempShift != 0) {
    tempShift += numbShift;
    shift(tempShift);
    ind -= 1;
  }
}

buttonRight.onclick = function () {
  const dayBlock = document.getElementsByClassName("day-block");
  if (ind != dayBlock.length - 4) {
    tempShift -= numbShift;
    shift(tempShift);
    ind += 1;
  }
}